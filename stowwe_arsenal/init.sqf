// Nathan Stowwe
// Arma 3 Lightweight Arsenals
// Initialization file
// 16-April-2020

call compile preprocessFile "stowwe_arsenal\config.sqf";
call compile preprocessFile "stowwe_arsenal\func.sqf";
stowwe_ars_initialized = true;
